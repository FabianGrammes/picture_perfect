from datetime import datetime
import os
from os.path import exists
import numpy as np
from PIL import Image
from pixellib.torchbackend.instance import instanceSegmentation

## set ut segmentation with pointrend model
ins = instanceSegmentation()
ins.load_model("pointrend_resnet50.pkl")

## loop over images and look for cattle
images = os.listdir('images')
target_classes = ins.select_target_classes(cow=True)
for img in images:
    imgfile='images/'+img
    segfile='segmented/'+img
    rgbfile='rgbtext/'+img.replace('.jpg','.txt')
    if not exists(rgbfile):
        print(datetime.now().strftime("%H:%M:%S")+" segmenting "+img)
        ## segment and save segmented image
        segmask, output = ins.segmentImage(imgfile, segment_target_classes= target_classes, show_bboxes=True, output_image_name=segfile,extract_segmented_objects= True)
        ## svae text file with RGB-values for pixels in mask
        image = np.asarray(Image.open(imgfile))
        bull = image.reshape((-1,3))[segmask['masks'][:,:,0].reshape((-1,1)).squeeze(),]
        np.savetxt(rgbfile,bull)
    else:
        print(datetime.now().strftime("%H:%M:%S ")+rgbfile," found , skipping ",img)
