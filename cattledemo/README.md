## Install Pixellib (https://github.com/ayoolaolafenwa/PixelLib#install-pixellib-and-its-dependencies)

# 1. Install Python >=3.7
conda create -n pointrend python=3.9
conda activate pointrend
# 2. Install Pytorch (https://pytorch.org/ -> install -> select: Stable, Linux, python, pip ,CPU )
pip3 install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cpu
# 3. Install pycocotools and pixellib
pip3 install pycocotools
pip3 install pixellib

## Download pointrend model 
## (https://github.com/ayoolaolafenwa/PixelLib/blob/master/Tutorials/Pytorch_image_instance_segmentation.md#instance-segmentation-of-images-in-pytorch)
wget https://github.com/ayoolaolafenwa/PixelLib/releases/download/0.2.0/pointrend_resnet50.pkl
#wget https://github.com/ayoolaolafenwa/PixelLib/releases/download/1.2/mask_rcnn_coco.h5 

## Download bull images 
mkdir -p images; cd images
wget https://www.geno.no/globalassets/bullimages/nrf/12222_nr-skoien.jpg
wget https://www.geno.no/globalassets/bullimages/nrf/12259-nr-rognin.jpg
wget https://www.geno.no/globalassets/bullimages/nrf/12252_nr-foss-p-et.jpg
cd ..

## Segmentation - find bulls in the images
mkdir -p segmented rgbtext
python segment.py
